package com.grupomns.GrupoMNS.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "VGF_PROD_APP")
public class Product {

  @Id
  @Column(name = "CODPROD")
  private int cod;

  @Column(name = "DESCRPROD")
  private String descricao;

  @Column(name = "DESCRPRODNFE")
  private String descrNFE;

  @Column(name = "EMBALAGEM")
  private String embalagem;

  @Column(name = "CODVOL")
  private String codVol;

}
