package com.grupomns.GrupoMNS.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
@Entity(name = "VGF_ITE_APP")
@IdClass(ProductHeaderId.class)
public class ProductHeader {

  @Id
  @Column(name = "CODPROD")
  private int cod;

  @Id
  @Column(name = "NUNOTA")
  private int nuNota;

  @Column(name = "DESCRPROD")
  private String descricao;

  @Column(name = "QTDNEG")
  private float qtdItens;

  @Column(name = "CODVOL")
  private String codVol;

  @Column(name = "CONTROLE")
  private String controle;

  @Column(name = "VLRUNIT")
  private float vlrUnit;

  @Column(name = "VLRTOTAL")
  private float vlrTotal;

  @Column(name = "AD_CODPROJ")
  private int adCodProj;

  @Column(name = "PROJETO")
  private String project;

  @Column(name = "CODVEND")
  private int codVend;

  @Column(name = "AD_VLRFRETEITEM")
  private float vlrFreteItem;

}
