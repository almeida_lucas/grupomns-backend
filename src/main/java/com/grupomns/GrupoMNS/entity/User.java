package com.grupomns.GrupoMNS.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "VGF_VENDUSU_APP")
public class User {

  @Id
  @Column(name = "CODUSU")
  private String code;

  @Column(name = "NOMEUSU")
  private String name;

  @Column(name = "APELIDO")
  private String nickname;

  @Column(name = "CODVEND")
  private String sellerCode;

  @Column(name = "SENHA")
  private String password;

}
