package com.grupomns.GrupoMNS.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "VGF_TIPONEG_APP")
public class OperationType {

  @Id
  @Column(name = "CODTIPOPER")
  private String codTipOper;

  @Column(name = "DESCRICAO")
  private String descricao;

}
