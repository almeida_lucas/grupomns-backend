package com.grupomns.GrupoMNS.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "VGF_PARC_APP")
public class Partner {

  @Id
  @Column(name = "CODPARC")
  private int code;

  @Column(name = "NOMEPARC")
  private String name;

  @Column(name = "CPF_CNPJ")
  private String cpfCNPJ;

  @Column(name = "CODTIPVENDA")
  private int sellTypeCode;

  @Column(name = "DESCRTIPVENDA")
  private String sellDescType;

  @Column(name = "CIDADE")
  private String city;

}
