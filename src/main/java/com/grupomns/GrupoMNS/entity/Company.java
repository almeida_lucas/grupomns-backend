package com.grupomns.GrupoMNS.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "VGF_EMP_APP")
public class Company {

  @Id
  @Column(name = "CODEMP")
  private int code;

  @Column(name = "NOMEFANTASIA")
  private String fantasyName;

  @Column(name = "CPF_CNPJ")
  private String cpfCNPJ;

  @Column(name = "CIDADE")
  private String city;

}
