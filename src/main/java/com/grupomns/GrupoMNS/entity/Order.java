package com.grupomns.GrupoMNS.entity;

import lombok.Data;

import java.util.List;

@Data
public class Order {
  private OrderHeader header;

  private List<ProductHeader> productHeaderList;
}
