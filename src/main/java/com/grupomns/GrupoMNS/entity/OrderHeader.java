package com.grupomns.GrupoMNS.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@Entity(name = "VGF_CAB_APP")
public class OrderHeader {

  private static final String ddMMyyyy = "dd/MM/yyyy";

  @Id
  @Column(name = "NUNOTA")
  private int nuNota;

  @Column(name = "NUMNOTA")
  private int numNota;

  @Column(name = "CODEMP")
  private int codEmp;

  @Column(name = "NOMEFANTASIA")
  private String nomeEmp;

  @Column(name = "CODPARC")
  private int codParc;

  @Column(name = "NOMEPARC")
  private String nomeParc;

  @Column(name = "CODVEND")
  private int codVend;

  @Column(name = "CODTIPOPER")
  private int codTipOper;

  @Column(name = "CODTIPVENDA")
  private int codTipVenda;

  @Column(name = "DESCRTIPVENDA")
  private String dscTipVenda;

  @Column(name = "DTNEG")
  private Date dtNeg;

  @Column(name = "VLRNOTA")
  private float vlrNota;

  @Column(name = "CODNAT")
  private int codNat;

  @Column(name = "CODCENCUS")
  private int codCencus;

  @Column(name = "CODPROJ")
  private int codProj;

  @Column(name = "OBS")
  private String observacao;

  @Column(name = "STATUSNOTA")
  private String statusNota;

  @Column(name = "NUAPP")
  private int nuApp;

  @Column(name = "CODUSU")
  private int codUsu;

  @Column(name = "NRO_NF")
  private String nroNF;

  @Column(name = "PENDENTE")
  private String pendente;

  @Column(name = "TIP_MOV")
  private String tipMov;

  public String getFormattedDateDDMMYYYY() {
    if (this.dtNeg == null)
      return null;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ddMMyyyy);

    return simpleDateFormat.format(this.dtNeg);
  }
}
