package com.grupomns.GrupoMNS.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "AD_VPRJATV")
public class Project {

  @Id
  @Column(name = "CODPROJ")
  private int code;

  @Column(name = "PROJETO")
  private String name;

}
